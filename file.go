package ezlog

import (
	"fmt"
	"os"
)

// FileLogger logs messages to a file
type FileLogger struct {
	f *os.File
}

// NewFileLogger opens a file to append to and
// returns a FileLogger ready to write to the file
func NewFileLogger(path string) (*FileLogger, error) {
	f, err := os.OpenFile(
		path,
		os.O_WRONLY|os.O_APPEND|os.O_CREATE,
		0644,
	)
	if err != nil {
		return nil, err
	}
	return &FileLogger{f}, nil
}

// Log logs a message to the file
func (l *FileLogger) Log(msg string) {
	fmt.Fprintln(l.f, msg)
}
