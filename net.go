package ezlog

import (
	"net"
)

type TCPLogger struct {
	conn *net.TCPConn
}

func (l *TCPLogger) Log(message string) {
	l.conn.Write([]byte(message))
	l.conn.Write([]byte("\n"))
}

func NewTCPLogger(address string) (*TCPLogger, error) {
	addr, err := net.ResolveTCPAddr("tcp", address)
	if err != nil {
		return nil, err
	}

	conn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		return nil, err
	}

	logger := TCPLogger{conn: conn}
	return &logger, nil
}
