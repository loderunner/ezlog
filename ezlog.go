// Package ezlog is a naive log implementation
// used as experiment and tutorial in learning Go
package ezlog

import (
	"fmt"
	"time"
)

type Logger interface {
	Log(string)
}

var loggers []Logger

// AddLogger adds a logger to the list
func AddLogger(l Logger) {
	loggers = append(loggers, l)
}

// Log logs a message to standard output preceded
// by the time the message was emitted
func Log(msg string) {
	// Format the output string
	now := time.Now()
	output := fmt.Sprintf("[%s] %s", now.Format("2006-01-02 03:04:05"), msg)

	// Log to all loggers
	for _, l := range loggers {
		l.Log(output)
	}
}
