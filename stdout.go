package ezlog

import (
	"fmt"
)

// StdoutLogger logs messages to stdout
type StdoutLogger struct{}

// NewStdoutLogger StdoutLogger constructor
func NewStdoutLogger() *StdoutLogger {
	return &StdoutLogger{}
}

// Log logs the msg to stdout
func (l *StdoutLogger) Log(msg string) {
	fmt.Println(msg)
}
