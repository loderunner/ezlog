package main

import "gitlab.com/loderunner/ezlog"

func main() {
	ezlog.AddLogger(ezlog.NewStdoutLogger())
	ezlog.Log("Hello World!")
}
