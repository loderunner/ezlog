package main

import (
	"fmt"
	"time"

	"gitlab.com/loderunner/ezlog"
)

func main() {
	// Log to stdout
	ezlog.AddLogger(ezlog.NewStdoutLogger())

	for now := range time.Tick(1 * time.Second) {
		// The seconds of the current time ends with 5
		if (now.Second() % 5) == 0 {
			// Add a new file to log to
			filename := fmt.Sprintf("gopher-%s.log", now.Format("030405"))
			fileLogger, err := ezlog.NewFileLogger(filename)
			if err == nil {
				ezlog.AddLogger(fileLogger)
			} else {
				ezlog.Log("Couldn't open new file.")
			}
		}

		ezlog.Log("Gopher!")
	}
}
