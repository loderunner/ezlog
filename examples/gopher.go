package main

import (
	"time"

	"gitlab.com/loderunner/ezlog"
)

func main() {
	// Add stdout logger
	ezlog.AddLogger(ezlog.NewStdoutLogger())

	// Add file logger
	fileLogger, err := ezlog.NewFileLogger("gopher.log")
	if err == nil {
		ezlog.AddLogger(fileLogger)
	} else {
		ezlog.Log("Couldn't open file.")
		ezlog.Log(err.Error())
	}

	// Add syslog logger
	syslogLogger, err := ezlog.NewSyslogLogger("gopher")
	if err == nil {
		ezlog.AddLogger(syslogLogger)
	} else {
		ezlog.Log("Couldn't open syslog.")
		ezlog.Log(err.Error())
	}

	// Add network logger
	tcpLogger, err := ezlog.NewTCPLogger("localhost:8901")
	if err == nil {
		ezlog.AddLogger(tcpLogger)
	} else {
		ezlog.Log("Couldn't open connection.")
		ezlog.Log(err.Error())
	}

	for _ = range time.Tick(1 * time.Second) {
		ezlog.Log("Gopher!")
	}
}
