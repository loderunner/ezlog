package ezlog

// #include <stdlib.h>
// #include <syslog.h>
// inline void syslog_wrapper(int priority, const char * message) {
//    syslog(priority, "%s", message);
// }
//
// inline int log_upto(int priority) {
//		return (LOG_UPTO(priority));
// }
import "C"

import (
	"unsafe"
)

type SyslogLogger struct{}

func (l *SyslogLogger) Log(message string) {
	cs := C.CString(message)
	defer C.free(unsafe.Pointer(cs))

	C.syslog_wrapper(C.LOG_CRIT, cs)
}

func NewSyslogLogger(identifier string) (*SyslogLogger, error) {
	cs := C.CString(identifier)
	defer C.free(unsafe.Pointer(cs))

	_, err := C.openlog(cs, 0, C.LOG_USER)
	if err != nil {
		return nil, err
	}

	prio, _ := C.log_upto(C.LOG_DEBUG)
	C.setlogmask(prio)

	return new(SyslogLogger), nil
}
